#!/usr/bin/env rakudo

use v6.d;

grammar DiceRoll {
    rule TOP { <dice> [ <bonus> ]? }

    token dice { <times> 'd' <die> }
    token times { \d+ }
    token die { \d+ }
    token bonus { <[+-]> \d+ }
}

class DiceRollActions {
    method TOP ($/) {
        my Int:D $total = [+] $<dice>.made;
        my Str:D $res = $<dice>.made.join(' + ');
        if ($<dice>.made.elems == 1 && !$<bonus>) { $res = "*$res*"; }

        # Handle optional bonus
        $total = ($total, $<bonus>).sum if $<bonus>;
        $res ~= $<bonus> if $<bonus>;

        $res ~= " = *$total*" if ($<dice>.made.elems != 1 || $<bonus>);
        make $res;
    }

    method dice ($/) {
        my @rolls;
        for ^+$<times> { @rolls.append((1..$<die>.Int).pick); }
        make @rolls;
    }
}

sub MAIN(
    Str:D $input = '1d20' #= Dice to roll. Must be in format: 1d20+7
) {
    my $res = DiceRoll.parse($input, :actions(DiceRollActions)).made;
    say "You roll a $input and get: $res" if $res;
    exit 1 if !$res;
}
